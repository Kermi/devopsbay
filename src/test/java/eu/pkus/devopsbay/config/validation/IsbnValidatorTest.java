package eu.pkus.devopsbay.config.validation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

class IsbnValidatorTest {

    private final IsbnValidator validator = new IsbnValidator();

    @Test
    void illegalNullAsIsbn() {
        assertThat(validator.isValid(null, null)).isFalse();
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "", // empty
            "1", // 1
            "123456789", // 9 digits
            "1234567890123", // 13 digits with wrong prefix
            "978456789012", // 12 digits with correct prefix
            "97945678901234", // 14 digits with correct prefix
            "979-45678-90-1-2-3", // 13 digits with too many separators
            "979-45678-90-1-23", // 13 digits with not separated checksum
            "ISBN1234567890", // 10 digits with ISBN prefix not space separated
            "ISBN-10 978 45678 90 12 3", // 13 digits with ISBN-10 prefix space separated
            "ISBN-10: 978 45678 90 12 3", // 13 digits with ISBN-10: prefix space separated
            "ISBN-13 1234567890", // 10 digits with ISBN-13 prefix space separated
            "ISBN-13: 1234567890", // 10 digits with ISBN-13: prefix space separated
            "ISBB-13: 978 45678 90 12 3", // 13 digits with ISBB-13: prefix space separated
            "ISBN-11: 978 45678 90 12 3", // 13 digits with ISBN-11: prefix space separated
            "ISBN-14: 978 45678 90 12 3", // 13 digits with ISBN-14: prefix space separated
            "ISBN-10: 1234567895", // 10 digits with wrong checksum
            "ISBN-13: 978 45678 90 12 2" // 13 digits with wrong checksum
    })
    void invalidIsbn(String isbn) {
        assertThat(validator.isValid(isbn, null)).isFalse();
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "123456789X", // 10 digits
            "9784567890120", // 13 digits with correct prefix
            "979-45678-90-12-9", // 13 digits with hyphen separators
            "978 45678 90 12 0", // 13 digits with space separators
            "ISBN 123456789X", // 10 digits with ISBN prefix space separated
            "ISBN-10 123456789X", // 10 digits with ISBN-10 prefix space separated
            "ISBN-10: 123456789X", // 10 digits with ISBN-10: prefix space separated
            "ISBN 978 45678 90 12 0", // 13 digits with ISBN prefix space separated
            "ISBN-13 978 45678 90 12 0", // 13 digits with ISBN-13 prefix space separated
            "ISBN-13: 978 45678 90 12 0" // 13 digits with ISBN-13: prefix space separated
    })
    void validIsbn(String isbn) {
        assertThat(validator.isValid(isbn, null)).isTrue();
    }

}