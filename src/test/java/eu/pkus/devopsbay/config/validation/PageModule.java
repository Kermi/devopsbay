package eu.pkus.devopsbay.config.validation;

// Copy paste -> https://stackoverflow.com/questions/52490399/spring-boot-page-deserialization-pageimpl-no-constructor

import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.data.domain.Page;

public class PageModule extends SimpleModule {
    private static final long serialVersionUID = 1L;

    public PageModule() {
        addDeserializer(Page.class, new PageDeserializer());
    }
}