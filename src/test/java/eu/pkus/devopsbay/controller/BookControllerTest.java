package eu.pkus.devopsbay.controller;

// Copy paste -> https://stackoverflow.com/questions/52490399/spring-boot-page-deserialization-pageimpl-no-constructor

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import eu.pkus.devopsbay.config.validation.PageModule;
import eu.pkus.devopsbay.model.command.BookCreateEditCommand;
import eu.pkus.devopsbay.model.command.CommentCreationCommand;
import eu.pkus.devopsbay.model.domain.Book;
import eu.pkus.devopsbay.model.domain.Comment;
import eu.pkus.devopsbay.model.view.BookView;
import eu.pkus.devopsbay.repository.BookRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc //need this in Spring Boot test
public class BookControllerTest {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private MockMvc mockMvc;

    private final ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

    @Test
    void addValidBook_then_bookIsAdded() throws Exception {
        // given
        var bookCreation = BookCreateEditCommand.builder()
                .title("testTitle")
                .author("testAuthor")
                .numberOfPages(50)
                .isbn("ISBN-13: 978 45678 90 12 0")
                .build();
        var expectedBook = Book.builder()
                .title("testTitle")
                .author("testAuthor")
                .numberOfPages(50)
                .isbn("ISBN-13: 978 45678 90 12 0")
                .build();

        // when
        var result = mockMvc.perform((MockMvcRequestBuilders.post("/books"))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(ow.writeValueAsString(bookCreation)))
                .andExpect(status().isOk())
                .andReturn();

        // then
        var resultBook = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Book.class);
        assertThat(resultBook)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(expectedBook);
    }

    @Test
    void addBookWithInvalidIsbn_then_errorMsgIsReturned() throws Exception {
        // given
        var bookCreation = BookCreateEditCommand.builder()
                .title("testTitle")
                .author("testAuthor")
                .numberOfPages(50)
                .isbn("ISBN-13: 978 45678 90 12 1")
                .build();

        // when
        mockMvc.perform((MockMvcRequestBuilders.post("/books"))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(ow.writeValueAsString(bookCreation)))
                // then
                .andExpect(status().is4xxClientError())
                .andExpect(content().string("[\"Not valid ISBN format," +
                        " please check https://www.isbn-international.org/content/what-isbn\"]"));
    }

    @Test
    void addBookWithEmptyTitle_then_errorMsgIsReturned() throws Exception {
        // given
        var bookCreation = BookCreateEditCommand.builder()
                .title("")
                .author("testAuthor")
                .numberOfPages(50)
                .isbn("ISBN-13: 978 45678 90 12 0")
                .build();

        // when
        mockMvc.perform((MockMvcRequestBuilders.post("/books"))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(ow.writeValueAsString(bookCreation)))
                // then
                .andExpect(status().is4xxClientError())
                .andExpect(content().string("[\"Title is required\"]"));
    }

    @Test
    void addBookWithEmptyAuthor_then_errorMsgIsReturned() throws Exception {
        // given
        var bookCreation = BookCreateEditCommand.builder()
                .title("testTitle")
                .author("")
                .numberOfPages(50)
                .isbn("ISBN-13: 978 45678 90 12 0")
                .build();

        // when
        mockMvc.perform((MockMvcRequestBuilders.post("/books"))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(ow.writeValueAsString(bookCreation)))
                // then
                .andExpect(status().is4xxClientError())
                .andExpect(content().string("[\"Author is required\"]"));
    }

    @Test
    void addBookWithPagesBelowZero_then_errorMsgIsReturned() throws Exception {
        // given
        var bookCreation = BookCreateEditCommand.builder()
                .title("testTitle")
                .author("testAuthor")
                .numberOfPages(-3)
                .isbn("ISBN-13: 978 45678 90 12 0")
                .build();

        // when
        mockMvc.perform((MockMvcRequestBuilders.post("/books"))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(ow.writeValueAsString(bookCreation)))
                // then
                .andExpect(status().is4xxClientError())
                .andExpect(content().string("[\"Number of pages cannot be negative value\"]"));
    }

    @Test
    void editBook_then_doNotLoseCommentData() throws Exception {
        // given
        var book = Book.builder()
                .title("testTitle")
                .author("testAuthor")
                .numberOfPages(42)
                .isbn("ISBN-13: 978 45678 90 12 0")
                .comment(Comment.builder()
                        .text("some comment")
                        .rating(4)
                        .build())
                .build();
        book = bookRepository.save(book);
        var expectedBook = book.toBuilder().author("new author").build();

        var bookEdit = BookCreateEditCommand.builder()
                .title("testTitle")
                .author("new author")
                .numberOfPages(42)
                .isbn("ISBN-13: 978 45678 90 12 0")
                .build();

        // when
        var result = mockMvc.perform((MockMvcRequestBuilders.put("/books/" + book.getId()))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(ow.writeValueAsString(bookEdit)))
                .andExpect(status().isOk())
                .andReturn();

        // then
        var resultBook = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Book.class);
        assertThat(resultBook)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(expectedBook);
    }

    @Test
    void editBookWithInvalidData_then_errorMsgIsReturned() throws Exception {
        // given
        var bookEdit = BookCreateEditCommand.builder()
                .title("testTitle")
                .author("new author")
                .numberOfPages(-42)
                .isbn("ISBN-13: 978 45678 90 12 0")
                .build();

        // when
        mockMvc.perform((MockMvcRequestBuilders.put("/books/test-id"))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(ow.writeValueAsString(bookEdit)))
                // then
                .andExpect(status().is4xxClientError())
                .andExpect(content().string("[\"Number of pages cannot be negative value\"]"));

    }

    @Test
    void editBookThatDoesNotExist_then_errorMsgNotFoundIsReturned() throws Exception {
        // given
        var bookEdit = BookCreateEditCommand.builder()
                .title("testTitle")
                .author("new author")
                .numberOfPages(42)
                .isbn("ISBN-13: 978 45678 90 12 0")
                .build();

        // when
        mockMvc.perform((MockMvcRequestBuilders.put("/books/test-id"))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(ow.writeValueAsString(bookEdit)))
                // then
                .andExpect(status().is4xxClientError())
                .andExpect(content().string("Book with ID: 'test-id' could not be found"));

    }

    @Test
    void findBookThatExists_then_fetchInformation() throws Exception {
        // given
        var book = Book.builder()
                .title("testTitle")
                .author("testAuthor")
                .numberOfPages(42)
                .isbn("ISBN-13: 978 45678 90 12 0")
                .comment(Comment.builder()
                        .text("some comment")
                        .rating(4)
                        .build())
                .build();
        book = bookRepository.save(book);

        // when
        var result = mockMvc.perform((MockMvcRequestBuilders.get("/books/" + book.getId())))
                .andExpect(status().isOk())
                .andReturn();

        // then
        var resultBook = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Book.class);
        assertThat(resultBook)
                .usingRecursiveComparison()
                .isEqualTo(book);
    }

    @Test
    void findBookThatDoesNotExist_then_errorMsgNotFoundIsReturned() throws Exception {
        // when
        mockMvc.perform((MockMvcRequestBuilders.get("/books/test-id")))
                // then
                .andExpect(status().is4xxClientError())
                .andExpect(content().string("Book with ID: 'test-id' could not be found"));

    }

    @Test
    void deleteBookThatExists_then_bookIsNoLongerPresent() throws Exception {
        // given
        var book = Book.builder()
                .title("testTitle")
                .author("testAuthor")
                .numberOfPages(42)
                .isbn("ISBN-13: 978 45678 90 12 0")
                .comment(Comment.builder()
                        .text("some comment")
                        .rating(4)
                        .build())
                .build();
        book = bookRepository.save(book);

        // when
        mockMvc.perform((MockMvcRequestBuilders.delete("/books/" + book.getId())))
                .andExpect(status().isOk())
                .andReturn();

        // then
        var result = bookRepository.existsById(book.getId());
        assertThat(result).isFalse();
    }

    @Test
    void deleteBookThatDoesNotExist_then_errorMsgNotFoundIsReturned() throws Exception {
        // when
        mockMvc.perform((MockMvcRequestBuilders.delete("/books/test-id")))
                // then
                .andExpect(status().is4xxClientError())
                .andExpect(content().string("Book with ID: 'test-id' could not be found"));

    }

    @Test
    void findMoreThan20Books_then_limitResultToPageSize() throws Exception {
        // given
        for (int i = 0; i < 30; i++) {
            bookRepository.save(Book.builder()
                    .title("testTitle")
                    .author("testAuthor")
                    .numberOfPages(42)
                    .isbn("ISBN-13: 978 45678 90 12 0")
                    .comment(Comment.builder()
                            .text("some comment")
                            .rating(4)
                            .build())
                    .build());
        }

        // when
        var result = mockMvc.perform((MockMvcRequestBuilders.get("/books")))
                .andExpect(status().isOk())
                .andReturn();

        // then
        var objMapper = new ObjectMapper();
        objMapper.registerModule(new PageModule());
        var resultPage = objMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Page<BookView>>() {
        });
        assertThat(resultPage).hasSize(20);
    }

    @Test
    void find3BooksWhenFiltering_then_resultOnly3() throws Exception {
        // given
        for (int i = 0; i < 30; i++) {
            bookRepository.save(Book.builder()
                    .title("testTitle")
                    .author("testAuthor")
                    .numberOfPages(42)
                    .isbn("ISBN-13: 978 45678 90 12 0")
                    .comment(Comment.builder()
                            .text("some comment")
                            .rating(4)
                            .build())
                    .build());
        }

        bookRepository.save(Book.builder()
                .title("Very real name")
                .author("testAuthor")
                .numberOfPages(42)
                .isbn("ISBN-13: 978 45678 90 12 0")
                .comment(Comment.builder()
                        .text("some comment")
                        .rating(4)
                        .build())
                .build());

        bookRepository.save(Book.builder()
                .title("It can also be real")
                .author("testAuthor")
                .numberOfPages(42)
                .isbn("ISBN-13: 978 45678 90 12 0")
                .comment(Comment.builder()
                        .text("some comment")
                        .rating(4)
                        .build())
                .build());

        bookRepository.save(Book.builder()
                .title("Realistic!")
                .author("testAuthor")
                .numberOfPages(42)
                .isbn("ISBN-13: 978 45678 90 12 0")
                .comment(Comment.builder()
                        .text("some comment")
                        .rating(4)
                        .build())
                .build());


        // when
        var result = mockMvc.perform((MockMvcRequestBuilders.get("/books?title=real")))
                .andExpect(status().isOk())
                .andReturn();

        // then
        var objMapper = new ObjectMapper();
        objMapper.registerModule(new PageModule());
        var resultPage = objMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Page<BookView>>() {
        });
        assertThat(resultPage).hasSize(3);
    }

    @Test
    void findBooksWithManyComments_then_resultShowsLast5() throws Exception {
        // given
        for (int i = 0; i < 30; i++) {
            bookRepository.save(Book.builder()
                    .title("testTitle")
                    .author("testAuthor")
                    .numberOfPages(42)
                    .isbn("ISBN-13: 978 45678 90 12 0")
                    .comment(Comment.builder()
                            .text("some comment")
                            .rating(3)
                            .build())
                    .comment(Comment.builder()
                            .text("some comment")
                            .rating(4)
                            .build())
                    .comment(Comment.builder()
                            .text("some comment")
                            .rating(5)
                            .build())
                    .comment(Comment.builder()
                            .text("some comment")
                            .rating(5)
                            .build())
                    .comment(Comment.builder()
                            .text("some comment")
                            .rating(7)
                            .build())
                    .comment(Comment.builder()
                            .text("some comment")
                            .rating(2)
                            .build())
                    .comment(Comment.builder()
                            .text("some comment")
                            .rating(1)
                            .build())
                    .comment(Comment.builder()
                            .text("some comment")
                            .rating(9)
                            .build())
                    .build());
        }

        // when
        var result = mockMvc.perform((MockMvcRequestBuilders.get("/books")))
                .andExpect(status().isOk())
                .andReturn();

        // then
        var objMapper = new ObjectMapper();
        objMapper.registerModule(new PageModule());
        var resultPage = objMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Page<BookView>>() {
        });
        assertThat(resultPage).hasSize(20);
        assertThat(resultPage.getContent().get(0).getComments()).hasSize(5);
    }

    @Test
    void addAComment_then_recalculateRating() throws Exception {
        // given
        var book = bookRepository.save(Book.builder()
                .title("specificTitle")
                .author("testAuthor")
                .numberOfPages(42)
                .isbn("ISBN-13: 978 45678 90 12 0")
                .comment(Comment.builder()
                        .text("some comment")
                        .rating(9)
                        .build())
                .comment(Comment.builder()
                        .text("some comment")
                        .rating(4)
                        .build())
                .comment(Comment.builder()
                        .text("some comment")
                        .rating(2)
                        .build())
                .comment(Comment.builder()
                        .text("some comment")
                        .rating(1)
                        .build())
                .comment(Comment.builder()
                        .text("some comment")
                        .rating(7)
                        .build())
                .build());

        var comment = CommentCreationCommand.builder().text("new comment").rating(3).build();

        // when
        mockMvc.perform((MockMvcRequestBuilders.post("/books/add-comment/" + book.getId()))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(ow.writeValueAsString(comment))
                )
                .andExpect(status().isOk())
                .andReturn();

        // then

        var result = mockMvc.perform((MockMvcRequestBuilders.get("/books?title=specific"))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(ow.writeValueAsString(comment))
                )
                .andExpect(status().isOk())
                .andReturn();
        var objMapper = new ObjectMapper();
        objMapper.registerModule(new PageModule());
        var resultPage = objMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Page<BookView>>() {
        });
        assertThat(resultPage).hasSize(1);
        assertThat(resultPage.getContent().get(0).getComments()).hasSize(5);
        assertThat(resultPage.getContent().get(0).getRating()).isEqualTo(4);
    }
}
