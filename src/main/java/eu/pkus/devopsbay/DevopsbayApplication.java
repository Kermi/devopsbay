package eu.pkus.devopsbay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevopsbayApplication {

    public static void main(String[] args) {
        SpringApplication.run(DevopsbayApplication.class, args);
    }

}
