package eu.pkus.devopsbay.service;

import eu.pkus.devopsbay.config.exception.BookNotFoundException;
import eu.pkus.devopsbay.config.mapper.BookMapper;
import eu.pkus.devopsbay.config.mapper.CommentMapper;
import eu.pkus.devopsbay.model.command.BookCreateEditCommand;
import eu.pkus.devopsbay.model.command.CommentCreationCommand;
import eu.pkus.devopsbay.model.command.SearchBookCommand;
import eu.pkus.devopsbay.model.domain.Book;
import eu.pkus.devopsbay.model.domain.Comment;
import eu.pkus.devopsbay.model.view.BookView;
import eu.pkus.devopsbay.repository.BookCustomRepository;
import eu.pkus.devopsbay.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BookService {

    private final BookRepository bookRepository;

    private final BookCustomRepository bookCustomRepository;

    public Book addBook(BookCreateEditCommand command) {
        var book = BookMapper.INSTANCE.bookCreationCommandToBook(command);
        return bookRepository.save(book);
    }

    public Book addComment(String id, CommentCreationCommand command) {
        var comment = CommentMapper.INSTANCE.commentCreationCommandToComment(command);

        var book = bookRepository.findById(id).orElseThrow(() -> new BookNotFoundException(id));
        var commentCount = book.getComments().size() + 1;
        var bookWithComment = book.toBuilder()
                .comment(comment)
                .rating((book.getComments().stream()
                        .map(Comment::getRating)
                        .reduce(0, Integer::sum) + comment.getRating()) / commentCount)
                .build();
        return bookRepository.save(bookWithComment);
    }

    public Book editBook(String id, BookCreateEditCommand command) {
        var book = bookRepository.findById(id).orElseThrow(() -> new BookNotFoundException(id));
        var editedBook = book.toBuilder()
                .title(command.getTitle())
                .author(command.getAuthor())
                .isbn(command.getIsbn())
                .numberOfPages(command.getNumberOfPages())
                .build();

        return bookRepository.save(editedBook);
    }

    public Book findById(String id) {
        return bookRepository.findById(id).orElseThrow(() -> new BookNotFoundException(id));
    }

    public Page<BookView> findBooks(SearchBookCommand filter, Pageable pageable) {
        return bookCustomRepository.findAllByFilter(filter, pageable)
                .map(BookMapper.INSTANCE::bookToBookView);
    }

    public void deleteBook(String id) {
        if (!bookRepository.existsById(id)) {
            throw new BookNotFoundException(id);
        }
        bookRepository.deleteById(id);
    }
}
