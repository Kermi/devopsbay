package eu.pkus.devopsbay.repository;

import eu.pkus.devopsbay.model.domain.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface BookRepository extends MongoRepository<Book, String> {

    @Query(value = "{}", fields = "{'comments': { '$slice': -5 } }")
    Page<Book> findAllWithLimitedComments(Pageable pageable);
}
