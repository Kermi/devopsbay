package eu.pkus.devopsbay.repository;

import eu.pkus.devopsbay.model.command.SearchBookCommand;
import eu.pkus.devopsbay.model.domain.Book;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.regex.Pattern;

@Service
@RequiredArgsConstructor
public class BookCustomRepository {

    private final MongoTemplate mongoTemplate;

    /**
     * Finds pageable result of {@link Book}s, also filters by fields defined in {@link SearchBookCommand},
     * uses Regex for strings and $gte $lte comparators for numbers,
     * null fields are not being filtered
     *
     * @param filter   fields to look for results
     * @param pageable range of results to display
     * @return {@link Page} of {@link Book}s matching criteria
     */
    public Page<Book> findAllByFilter(SearchBookCommand filter, Pageable pageable) {
        Criteria criteria = buildCriteria(filter);

        var match = Aggregation.match(criteria);

        var skip = Aggregation.skip((long) pageable.getPageNumber() * pageable.getPageSize());
        var limit = Aggregation.limit(pageable.getPageSize());

        var slice = Aggregation.project().and("comments").slice(-5).as("comments")
                .andInclude("id", "title", "author", "rating", "isbn", "numberOfPages")
                .and("referenceNumber").as("reference_number");

        var count = mongoTemplate
                .query(Book.class)
                .matching(criteria)
                .count();


        var aggregation = Aggregation.newAggregation(
                match,
                skip,
                limit,
                slice
        );

        var results = new ArrayList<>(mongoTemplate
                .aggregate(aggregation, "books", Book.class)
                .getMappedResults());

        return new PageImpl<>(results, pageable, count);
    }

    private Criteria buildCriteria(SearchBookCommand filter) {
        var criteriaBuilder = new ArrayList<Criteria>();

        if (StringUtils.hasLength(filter.getTitle())) {
            criteriaBuilder.add(regexCriteria("title", filter.getTitle()));
        }
        if (StringUtils.hasLength(filter.getAuthor())) {
            criteriaBuilder.add(regexCriteria("author", filter.getAuthor()));
        }
        if (StringUtils.hasLength(filter.getIsbn())) {
            criteriaBuilder.add(regexCriteria("isbn", filter.getIsbn()));
        }
        if (filter.getMinPages() != null) {
            criteriaBuilder.add(Criteria.where("numberOfPages").gte(filter.getMinPages()));
        }
        if (filter.getMinPages() != null) {
            criteriaBuilder.add(Criteria.where("numberOfPages").lte(filter.getMaxPages()));
        }
        if (filter.getMinPages() != null) {
            criteriaBuilder.add(Criteria.where("rating").gte(filter.getMinRating()));
        }
        if (filter.getMinPages() != null) {
            criteriaBuilder.add(Criteria.where("rating").lte(filter.getMaxRating()));
        }

        if (!CollectionUtils.isEmpty(criteriaBuilder)) {
            if (criteriaBuilder.size() == 1) {
                return criteriaBuilder.get(0);
            } else {
                return new Criteria().andOperator(criteriaBuilder.toArray(new Criteria[0]));
            }
        }

        return new Criteria();
    }

    private Criteria regexCriteria(String field, String value) {
        return Criteria.where(field).regex(Pattern.compile(value, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE));
    }


}
