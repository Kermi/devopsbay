package eu.pkus.devopsbay.controller;

import eu.pkus.devopsbay.model.command.BookCreateEditCommand;
import eu.pkus.devopsbay.model.command.CommentCreationCommand;
import eu.pkus.devopsbay.model.command.SearchBookCommand;
import eu.pkus.devopsbay.model.domain.Book;
import eu.pkus.devopsbay.model.view.BookView;
import eu.pkus.devopsbay.service.BookService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Main controller for the application, covers all business logic, creates/modifies/deletes books
 */
@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
@Validated
public class BookController {

    private final BookService bookService;

    @PostMapping
    @ApiOperation(value = "Create a book",
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Book has been created", response = Book.class),
    })
    Book addBook(@Valid @RequestBody BookCreateEditCommand command) {
        return bookService.addBook(command);
    }

    @PostMapping("add-comment/{bookId}")
    @ApiOperation(value = "Add a comment to given book",
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Comment has been added", response = Book.class),
    })
    Book addComment(@PathVariable String bookId, @Valid @RequestBody CommentCreationCommand command) {
        return bookService.addComment(bookId, command);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Edit a book",
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Book has been modified", response = Book.class),
            @ApiResponse(code = 404, message = "Book does not exist with given ID", response = Book.class),
    })
    Book editBook(@PathVariable String id, @Valid @RequestBody BookCreateEditCommand command) {
        return bookService.editBook(id, command);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find a book by ID",
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Book has been found", response = Book.class),
            @ApiResponse(code = 404, message = "Book does not exist with given ID", response = Book.class),
    })
    Book findBookById(@PathVariable String id) {
        return bookService.findById(id);
    }

    @GetMapping()
    @ApiOperation(value = "Find page of books with filter, limit comments for each book to max 5 last",
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Page of books, matching criteria filter", response = Book.class),
    })
    Page<BookView> findBooks(Pageable pageable, SearchBookCommand filter) {
        return bookService.findBooks(filter, pageable);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete a book",
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Book has been deleted", response = Book.class),
            @ApiResponse(code = 404, message = "Book does not exist with given ID", response = Book.class),
    })
    void deleteBook(@PathVariable String id) {
        bookService.deleteBook(id);
    }

}
