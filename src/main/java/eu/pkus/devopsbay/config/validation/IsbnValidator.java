package eu.pkus.devopsbay.config.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class IsbnValidator implements ConstraintValidator<IsbnValidation, String> {

    private static final Pattern ISBN_10_PATTERN = Pattern.compile(
            "^(?:ISBN(?:-10)?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$)" +
                    "[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$");
    private static final Pattern ISBN_13_PATTERN = Pattern.compile(
            "^(?:ISBN(?:-13)?:? )?(?=[0-9]{13}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)" +
                    "97[89][- ]?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9]$");

    private static final String PREFIXES_AND_SEPARATORS_TO_REMOVE = "^(?:ISBN(?:-10)?:? )|^(?:ISBN(?:-13)?:? )| |-";

    /**
     * Checks if tested value is a correct ISBN, when reges is matched then checksum is being calculated
     *
     * @param value to be checked if valid ISBN
     * @param cxt   context
     * @return boolean value, true if is ISBN, otherwise false
     */
    public boolean isValid(String value, ConstraintValidatorContext cxt) {

        if (value == null) {
            return false;
        }

        var pureIsbn = value.replaceAll(PREFIXES_AND_SEPARATORS_TO_REMOVE, "");
        var pureIsbnLength = pureIsbn.length();
        if (pureIsbnLength != 10 && pureIsbnLength != 13) {
            return false;
        }

        if (ISBN_10_PATTERN.matcher(value).matches()) {
            var checksum = pureIsbn.charAt(9);
            var sum = 0;
            for (int i = 0; i < pureIsbn.length() - 1; i++) {
                sum += (10 - i) * Integer.parseInt(String.valueOf(pureIsbn.charAt(i))); // -1 to skip checksum
            }
            var secondToLastResult = 11 - (sum % 11);
            var calculatedChecksum = secondToLastResult == 10 ? "X" : String.valueOf(secondToLastResult);
            return checksum == calculatedChecksum.charAt(0);

        } else if (ISBN_13_PATTERN.matcher(value).matches()) {
            var checksum = pureIsbn.charAt(12);
            var sum = 0;
            for (int i = 0; i < pureIsbn.length() - 1; i++) {
                sum += ((i % 2) * 2 + 1) * Integer.parseInt(String.valueOf(pureIsbn.charAt(i)));
            }
            var calculatedChecksumNumber = (10 - (sum % 10)) % 10;
            var calculatedChecksum = String.valueOf(calculatedChecksumNumber);
            return checksum == calculatedChecksum.charAt(0);
        }
        return false;
    }

}