package eu.pkus.devopsbay.config.exception;

public class BookNotFoundException extends RuntimeException {

    public BookNotFoundException(String id) {
        super(String.format("Book with ID: '%s' could not be found", id));
    }

}
