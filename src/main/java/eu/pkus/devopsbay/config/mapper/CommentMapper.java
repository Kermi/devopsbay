package eu.pkus.devopsbay.config.mapper;

import eu.pkus.devopsbay.model.command.CommentCreationCommand;
import eu.pkus.devopsbay.model.domain.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CommentMapper {

    CommentMapper INSTANCE = Mappers.getMapper(CommentMapper.class);

    Comment commentCreationCommandToComment(CommentCreationCommand command);

}