package eu.pkus.devopsbay.config.mapper;

import eu.pkus.devopsbay.model.command.BookCreateEditCommand;
import eu.pkus.devopsbay.model.domain.Book;
import eu.pkus.devopsbay.model.view.BookView;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface BookMapper {

    BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);

    // @Mapping(source = "status", target = "orderStatus")
    Book bookCreationCommandToBook(BookCreateEditCommand command);

    BookView bookToBookView(Book book);

}