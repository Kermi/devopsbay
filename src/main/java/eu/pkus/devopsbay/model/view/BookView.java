package eu.pkus.devopsbay.model.view;

import eu.pkus.devopsbay.model.domain.Comment;
import lombok.*;
import org.springframework.data.annotation.Id;

import java.util.List;

@Builder(toBuilder = true)
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class BookView {

    @Id
    private String id;

    private String title;
    private String author;
    private Integer numberOfPages;
    private Integer rating;
    private String isbn;

    @Singular
    private List<Comment> comments;

}
