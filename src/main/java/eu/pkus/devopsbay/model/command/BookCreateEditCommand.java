package eu.pkus.devopsbay.model.command;

import eu.pkus.devopsbay.config.validation.IsbnValidation;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class BookCreateEditCommand {

    @NotBlank(message = "Title is required")
    @ApiModelProperty(value = "Title of a book to create/edit",
            name = "title",
            example = "The Lord of the Rings",
            dataType = "String",
            required = true,
            position = 0)
    private String title;

    @NotBlank(message = "Author is required")
    @ApiModelProperty(value = "Author of a book to create/edit",
            name = "author",
            example = "R. R. Tolkien",
            dataType = "String",
            required = true,
            position = 1)
    private String author;

    @Min(value = 0, message = "Number of pages cannot be negative value")
    @ApiModelProperty(value = "Number of pages of a book to create/edit",
            name = "title",
            example = "653",
            dataType = "Integer",
            required = true,
            position = 2)
    private Integer numberOfPages;

    @IsbnValidation(message = "Not valid ISBN format, please check https://www.isbn-international.org/content/what-isbn")
    @ApiModelProperty(value = "ISBN of a book to create/edit",
            name = "isbn",
            example = "9780395647387",
            dataType = "String",
            required = true,
            position = 3)
    private String isbn;

}
