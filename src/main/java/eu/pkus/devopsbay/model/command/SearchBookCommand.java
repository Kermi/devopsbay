package eu.pkus.devopsbay.model.command;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SearchBookCommand {

    @ApiModelProperty(value = "Regex field to search by a title",
            name = "title",
            example = "The Lord of the R",
            dataType = "String",
            required = false,
            position = 0)
    private String title;

    @ApiModelProperty(value = "Regex field to search by an author",
            name = "author",
            example = "Tolkien",
            dataType = "String",
            required = false,
            position = 0)
    private String author;

    @ApiModelProperty(value = "Regex field to search by an ISBN",
            name = "isbn",
            example = "989",
            dataType = "String",
            required = false,
            position = 0)
    private String isbn;

    @ApiModelProperty(value = "Min number of pages to search for",
            name = "minPages",
            example = "0",
            dataType = "Integer",
            required = false,
            position = 0)
    private Integer minPages;

    @ApiModelProperty(value = "Max number of pages to search for",
            name = "maxPages",
            example = "50",
            dataType = "Integer",
            required = false,
            position = 0)
    private Integer maxPages;

    @ApiModelProperty(value = "Min rating to search for",
            name = "minRating",
            example = "0",
            dataType = "Integer",
            required = false,
            position = 0)
    private Integer minRating;

    @ApiModelProperty(value = "Max rating to search for",
            name = "maxRating",
            example = "0",
            dataType = "Integer",
            required = false,
            position = 0)
    private Integer maxRating;

}
