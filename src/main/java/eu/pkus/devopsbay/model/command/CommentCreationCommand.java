package eu.pkus.devopsbay.model.command;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class CommentCreationCommand {

    @NotBlank
    @ApiModelProperty(value = "Text of a comment",
            name = "text",
            example = "It is the comment for a given book",
            dataType = "String",
            required = true,
            position = 0)
    private String text;

    @Min(0)
    @ApiModelProperty(value = "Rating of a book",
            name = "rating",
            example = "5",
            dataType = "Integer",
            required = false,
            position = 1)
    private Integer rating;

}
