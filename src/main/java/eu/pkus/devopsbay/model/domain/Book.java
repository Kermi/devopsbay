package eu.pkus.devopsbay.model.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "books")
@Builder(toBuilder = true)
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Book {

    @Id
    private String id;

    @ApiModelProperty(value = "Title of a book",
            name = "title",
            example = "The Lord of the Rings",
            dataType = "String",
            required = true,
            position = 0)
    private String title;

    @ApiModelProperty(value = "Author of a book",
            name = "author",
            example = "R. R. Tolkien",
            dataType = "String",
            required = true,
            position = 1)
    private String author;

    @ApiModelProperty(value = "Number of pages of a book",
            name = "title",
            example = "653",
            dataType = "Integer",
            required = true,
            position = 2)
    private Integer numberOfPages;

    @ApiModelProperty(value = "Rating of a book",
            name = "rating",
            example = "6",
            dataType = "String",
            required = true,
            position = 3)
    private Integer rating;

    @ApiModelProperty(value = "ISBN of a book",
            name = "isbn",
            example = "9780395647387",
            dataType = "String",
            required = true,
            position = 4)
    private String isbn;

    @ApiModelProperty(value = "Comments of a book",
            name = "comments",
            example = "[{'text': 'Valuable comment', 'rating': 4}]",
            dataType = "String",
            required = true,
            position = 3)
    @Singular
    private List<Comment> comments;

}
