package eu.pkus.devopsbay.model.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Document(collection = "comments")
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Comment {

    @Builder.Default
    private String id = UUID.randomUUID().toString();

    @ApiModelProperty(value = "Text of a comment",
            name = "text",
            example = "It is the comment for a given book",
            dataType = "String",
            required = true,
            position = 0)
    private String text;

    @ApiModelProperty(value = "Rating of a book",
            name = "rating",
            example = "5",
            dataType = "Integer",
            required = false,
            position = 1)
    private Integer rating;

}
