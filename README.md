# devopsbay

## Requirements
- [Docker](https://docs.docker.com/get-docker/)
- [JDK17](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html)
- [Maven](https://maven.apache.org/download.cgi)

## Getting started

Before an application can be started, database needs to be set, it has been scripted so running a command 

`docker-compose up -d devopsbay-mongodb` 

from docker-compose.yml file level should be enough to start a database and configure it. 

Next step should be building a jar from sources by running command `mvn clean package`


As everything is ready to start an app, call from top of the project `mvn spring-boot:run`

## Running
Application can be reached at `http://localhost:8080`, swagger documentation has been added so API can be 
triggered through

[Swagger UI](http://localhost:8080/swagger-ui/)